const urlencoded = require('body-parser').urlencoded
const email = require('@sendgrid/mail')

email.setApiKey(process.env.SENDGRID_API_KEY)

// https://stackoverflow.com/questions/4810841/how-can-i-pretty-print-json-using-javascript
function syntaxHighlight(json) {
    if (typeof json != 'string') {
         json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

module.exports = (req, res) => {
  const next = async () => {
    let body = Object.assign({}, req.body)

    body = JSON.stringify(body, null, 2)

    const message = {
      from: 'ErrorBot <errors@qnzl.co>',
      to: 'dev@qnzl.co',
      subject: 'Something bad is happening with PhonePrivacy!',
      text: body,
      html: syntaxHighlight(body)
    }

    await email.send(message)

    return res.status(200).send('')
  }

  urlencoded()(req, res, next)

}

