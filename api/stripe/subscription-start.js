const email = require('@sendgrid/mail')

email.setApiKey(process.env.SENDGRID_API_KEY)

module.exports = (req, res) => {
  // TODO Check if coming from stripe

  const {
    type,
    data: {
      object: {
        plan
      }
    }
  } = req.body

  if (type === `customer.subscription.created`) {
    const message = {
      from: `SubscriptionBot <subscriptions@qnzl.co>`,
      to: `billing@qnzl.co`,
      subject: `Subscription to ${plan.nickname}`,
      text: `Get going! :)`
    }

    console.log("Trying to send subscription start")
    email.send(message)
      .then(() => {
        console.log("Sending subscription start")
        return res.status(200).send()
      })
  } else {
    return res.status(200).send()
  }
}
