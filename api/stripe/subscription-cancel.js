const email = require('@sendgrid/mail')

email.setApiKey(process.env.SENDGRID_API_KEY)

module.exports = (req, res) => {
  const {
    type,
    data: {
      object: {
        plan
      }
    }
  } = req.body

  if (type === `customer.subscription.deleted`) {
    const message = {
      from: `SubscriptionBot <subscriptions@qnzl.co>`,
      to: `billing@qnzl.co`,
      subject: `Someone cancelled! :(`,
      text: `${plan.nickname} plan was cancelled`
    }

    email.send(message)
      .then(() => {
        return res.status(200).send()
      })
  } else {
    return res.status(200).send()
  }
}

